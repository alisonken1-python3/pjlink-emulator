# pjlink-server

## Emulate a PJLink projector for testing client software.

PJLink is a standard maintained by Japan Business Machines and
Information Systems Industries Association (JBMIA) for remote network
control of projectors.

To be compatible with the widest variations, JPLink only provides the
necessary controls for basic settings (power, shutter, video source,
microphone/speaker, error/warning status).

Some additional information about the projector is also available.

## License / Notes

Licensed under GNU General Public License v2.

Copyright (c) Ken Robers, 2017.

Some functionality borrowed from OpenLP.

Main formatting will be in style of Pep8.

Exceptions to Pep8:
* Line length for non-docstrings 120 characters

### PJLink Resources

* PJLink Class 1 dated 2013-12-10
* PJLink Class 2 dated 2017-01-31

### Main Dependencies

* Linux
* Qt5
* Python 3.5+
* PyQt5

### Optional Functionality

* Nose2 (testing)
* Python3 Pep8/Flake8 (Code formatting)

### References

* Python http://python.org
* PyQt https://riverbankcomputing.com/software/pyqt
* JBMIA PJLink http://pjlink.jbmia.or.jp/english/
* PJLink Class 1 http://pjlink.jbmia.or.jp/english/dl_class1.html
* PJLink Class 2 http://pjlink.jbmia.or.jp/english/dl_class2.html
* OpenLP https://openlp.org
* Flake8/PyCodeStyle error codes https://pycodestyle.readthedocs.io/en/latest/intro.html#error-codes
* Pep8 Code Style https://www.python.org/dev/peps/pep-0008/
