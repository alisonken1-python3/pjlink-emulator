# -*- coding: utf-8 -*-
# vim: autoindent shiftwidth=4 expandtab textwidth=120 tabstop=4 softtabstop=4

###############################################################################
# PJLink Emulator - Emulate a PJLink terminal                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2017 Ken Roberts                                              #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 59  #
# Temple Place, Suite 330, Boston, MA 02111-1307 USA                          #
###############################################################################
"""
The :mod:`pjlink.common` module provides the common functions that may be used throughout the program.
"""

import fcntl
import hashlib
import logging
import os
import socket
import sys
import struct

from ipaddress import IPv4Address, IPv6Address, AddressValueError

log = logging.getLogger(__name__)


def getHwAddr(ifname):
    """
    Returns the ethernet address specified by 'ifname' formatted as 'xx:xx:xx:xx:xx:xx'.

    :param ifname: (String) Interface to check
    :returns: (String) Ethernet address
    """
    req = 0x8927  # Not sure about how this number is defined - something to do with termios info?
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # Pack the IFName into a structure that fcntl.ioctl can use
    ifpacked = struct.pack('256s', bytearray(ifname[:15], encoding='utf-8'))
    # We only want bytes 18:24 of the returned bytearray, but also need
    # to convert it to a string using the b.hex() function
    try:
        info = fcntl.ioctl(sock, req, ifpacked)[18:24].hex()
        mac = ':'.join(a + b for a, b in zip(info[::2], info[1::2]))
    except OSError as err:
        # If ifname is not available, log as an error and return an empty string
        if err.errno == 19:
            log.error("getHwAddr('{name}') Interface not available".format(name=ifname))
            return ''
        else:
            # For other errors, reraise the error
            raise
    # Return the results as a proper ethernet hardware address string representation
    log.debug("getHwAddr('{name}') returning '{mac}'".format(name=ifname, mac=mac))
    return mac


def is_linux():
    """
    Returns true if running on a system with a linux kernel e.g. Ubuntu, Debian, etc

    :return: True if system is running a linux kernel false otherwise
    """
    return sys.platform.startswith('linux')


def is_macosx():
    """
    Returns true if running on a system with a darwin kernel e.g. Mac OS X

    :return: True if system is running a darwin kernel false otherwise
    """
    return sys.platform.startswith('darwin')


def is_win():
    """
    Returns true if running on a system with a nt kernel e.g. Windows, Wine

    :return: True if system is running a nt kernel false otherwise
    """
    return os.name.startswith('nt')


def md5_hash(data, salt=None):
    """
    Returns the hashed output of md5sum using Python3 hashlib

    :param data: (String) Data to hash
    :param salt: (String) OPTIONAL Initial salt
    :returns: (String) MD5Sum hash
    """
    log.debug('md5_hash(salt="{text}")'.format(text=salt))
    hash_obj = hashlib.new('md5')
    if salt:
        hash_obj.update(salt)
    if data:
        hash_obj.update(data)
    hash_value = hash_obj.hexdigest()
    log.debug('md5_hash() returning "{text}"'.format(text=hash_value))
    return hash_value


def verify_ipv4(addr):
    """
    Validate an IPv4 address

    :param addr: Address to validate
    :returns: bool
    """
    try:
        IPv4Address(addr)
        return True
    except AddressValueError:
        return False


def verify_ipv6(addr):
    """
    Validate an IPv6 address

    :param addr: Address to validate
    :returns: bool
    """
    try:
        IPv6Address(addr)
        return True
    except AddressValueError:
        return False


def verify_ip_address(addr):
    """
    Validate an IP address as either IPv4 or IPv6

    :param addr: Address to validate
    :returns: bool
    """
    return True if verify_ipv4(addr) else verify_ipv6(addr)
