#!/usr/bin/env bash

$( cd ../documentation/markdown && \

pandoc -o ../pdf/pjlink-emulator.pdf --normalize --toc -V links-as-notes -V lof -V lot \
    000/metadata.yaml \
    000/0001-introduction.mmd \
    001/0010-section.mmd \
    001/0011-keywords.mmd \
    002/0020-section.mmd \
    002/0021-protocol.mmd \
    appendix/A00-section.mmd \
    appendix/A01-defines.mmd \
    appendix/A02-acknowledge.mmd \
    appendix/A03-reference.mmd\
    appendix/A19-rfc2119.mmd

)
echo "pdf has been saved under documentation/pdf/pjlink-emulator.pdf"

